<?php
    include("header.php");
    
    if (!isset($_SESSION['id'])) {
        die('Bitte zuerst <a href="login.php">einloggen</a>');
    }
    
    echo "<h1>Mahlzeiten</h1>";
    
    // TODO: Create table with numbers at the bottom or top for how many C, V and Co's are there

    $projectName = "Randa Meetings 2017";

    $statement = $pdo->prepare("SELECT startDate, endDate FROM tbl_projects WHERE name = ?");
    $result = $statement->execute(array($projectName));
    
    if ($result !== false) {
        $projectDates = $statement->fetch();
    } else {
        die("<p>No project found with name $projectName</p>");
    }
    
    // FIXME: Must be easier to accomplish ;-)
    $startString = $projectDates['startDate'];
    $endString = $projectDates['endDate'];
    
    // FIXME: Tricky part "-"-separator, depends on locale, please fix and change and fix
    $startArray = explode("-", $startString);
    $endArray = explode("-", $endString);
    
    $startTime = mktime(0, 0, 0, $startArray[1], $startArray[2], $startArray[0]);
    $endTime = mktime(0, 0, 0, $endArray[1], $endArray[2], $endArray[0]);
    
    $startDate = getDate($startTime);
    $endDate = getDate($endTime);
    
    $days = date_diff(date_create($startString), date_create($endString));
    $dayNo = intval($days->format('%a'));
    
    $foodTableName = "tbl_meals";
    $totalMealsArray = array();
    
    // Participation table
    echo "<table border='1'>";
    
    echo "<tr>";
    echo "<td /><td />";   
    $currentDate = getDate($startTime);
    for ($i = 0; $i <= $dayNo; $i++) {
        $currentDate = getDate($startTime + $i*86400);
        $weekday = $currentDate['weekday'];
        echo "<td align='center'>" . $weekday . "</td>";
    }
    echo "</tr>";
    
    echo "<tr>";
    echo "<td>Vorname</td>";
    echo "<td>Nachname</td>";
    $currentTime = $startTime;
    for ($i = 1; $i <= $dayNo+1; $i++) {
        echo "<td>" . date('d.m.Y', $currentTime + ($i-1)*86400) . "</td>";
        $breakfastIndex = "day" . $i . "_B";
        $lunchIndex = "day" . $i . "_L";
        $dinnerIndex = "day" . $i . "_D";
//         echo "<p>Bla $breakfastIndex</p>";
        $totalMealsArray[$breakfastIndex . "_C"] = 0;
        $totalMealsArray[$breakfastIndex . "_V"] = 0;
        $totalMealsArray[$lunchIndex . "_C"] = 0;
        $totalMealsArray[$lunchIndex . "_V"] = 0;
        $totalMealsArray[$dinnerIndex . "_C"] = 0;
        $totalMealsArray[$dinnerIndex . "_V"] = 0;
    }
    echo "</tr>";

//     echo "<p>" . print_r($totalMealsArray) . "</p>";
    
    
    $statement = $pdo->prepare("SELECT * FROM tbl_participants INNER JOIN $foodTableName ON tbl_participants.userid = $foodTableName.userid");
    $statement->execute();
    
    $participantIndex = 0;
    
    while ($participant = $statement->fetch()) {
        $participantIndex++;
        if ($participantIndex%2 != 0) {
            echo "<tr bgcolor='lightgray'>";
        } else {
            echo "<tr>";
        }
        $userid = $participant['userid'];
        
        echo "<td><a href='participants.php?userid=$userid&mode=details'>" . $participant['firstname'] . "</a></td>";
        echo "<td><a href='participants.php?userid=$userid&mode=details'>" . $participant['lastname'] . "</a></td>";
        for ($i = 1; $i <= $dayNo+1; $i++) {
            echo "<td align='center'>";
            $breakfastIndex = "day$i" . "_B";
            $lunchIndex = "day$i" . "_L";
            $dinnerIndex = "day$i" . "_D";
            $breakfastMeal = $participant[$breakfastIndex];
            $lunchMeal = $participant[$lunchIndex];
            $dinnerMeal = $participant[$dinnerIndex];
            
            // FIXME: This code (calculation of number of meals per foodPreference) should happen
            // (and updated if user data changes) somewhere else and just be showed here
            switch ($breakfastMeal) {
                case "C":
                    $totalMealsArray[$breakfastIndex . "_C"]++;
                    break;
                case "V":
                    $totalMealsArray[$breakfastIndex . "_V"]++;
                    break;                        
            }
            
            switch ($lunchMeal) {
                case "C":
                    $totalMealsArray[$lunchIndex . "_C"]++;
                    break;
                case "V":
                    $totalMealsArray[$lunchIndex . "_V"]++;
                    break;                        
            }
            
            switch ($dinnerMeal) {
                case "C":
                    $totalMealsArray[$dinnerIndex . "_C"]++;
                    break;
                case "V":
                    $totalMealsArray[$dinnerIndex . "_V"]++;
                    break;                        
            }

            echo $breakfastMeal . " ";
            echo $lunchMeal . " ";
            echo $dinnerMeal . " ";
            echo "</td>";
            
        }
        echo "</tr>";
    }
    
    echo "<tr><td>TOTAL</td><td>Carnivores</td>";
    for ($i = 1; $i <= $dayNo+1; $i++) {
        echo "<td align='center'>";
        echo $totalMealsArray["day" . $i . "_B_C"] . " ";
        echo $totalMealsArray["day" . $i . "_L_C"] . " ";
        echo $totalMealsArray["day" . $i . "_D_C"];
        echo "</td>";
    }
    echo "</tr>";
    
    
    echo "<tr><td>TOTAL</td><td>Vegetarians</td>";
    for ($i = 1; $i <= $dayNo+1; $i++) {
        echo "<td align='center'>";
        echo $totalMealsArray["day" . $i . "_B_V"] . " ";
        echo $totalMealsArray["day" . $i . "_L_V"] . " ";
        echo $totalMealsArray["day" . $i . "_D_V"];
        echo "</td>";
    }
    echo "</tr>";
    
    // TODO: Create the belowen thing ;-)
//     echo "<tr><td>TOTAL</td><td>All</td></tr>";
    
    
    echo "</table>";   
    
    include("footer.php");
?>
