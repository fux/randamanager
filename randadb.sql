
CREATE USER 'randauser'@'localhost' IDENTIFIED BY 'rnd2017?!';
CREATE DATABASE IF NOT EXISTS randadb;
GRANT ALL PRIVILEGES ON randadb.* TO 'randauser'@'localhost' IDENTIFIED BY 'rnd2017?!';

USE randadb;

--
-- Table structure for table `tbl_rooms`
--

CREATE TABLE `tbl_rooms` (
  `roomNo` int(11) NOT NULL,
  `roomFloor` int(11) NOT NULL,
  `bedNo` int(11) NOT NULL,
  `freeBeds` int(11) NOT NULL,
  `roomType` varchar(50) DEFAULT NULL,
  `roomName` varchar(20) DEFAULT NULL,
  `roomStatus` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tbl_rooms` ADD PRIMARY KEY (`roomNo`);

--
-- Dumping data for table `tbl_rooms`
--

INSERT INTO `tbl_rooms` (`roomNo`, `roomFloor`, `bedNo`, `freeBeds`, `roomType`, `roomName`, `roomStatus`) VALUES
(0, 1, 0, 0, 'specialroom', 'Registration Office', 'occupied'),
(1, 1, 0, 0, 'grouproom', 'Asia', 'free'),
(2, 2, 0, 0, 'grouproom', 'America', 'free'),
(3, 3, 0, 0, 'grouproom', 'America', 'free'),
(4, 4, 0, 0, 'grouproom', 'Asia', 'free'),
(5, 4, 0, 0, 'grouproom', 'Europe', 'free'),
(6, 0, 0, 0, 'dinningroom', '', 'free'),
(7, -1, 0, 0, 'dinningroom', '', 'free'),
(8, -1, 0, 0, 'specialroom', 'Play Room', 'free'),
(9, -1, 0, 0, 'specialroom', 'Kitchen', 'free'),
(10, 1, 3, 3, 'bedroom', '', 'free'),
(11, 1, 1, 1, 'bedroom', '', 'free'),
(12, 1, 1, 1, 'bedroom', '', 'free'),
(14, 1, 1, 1, 'bedroom', '', 'free'),
(15, 1, 1, 1, 'bedroom', '', 'free'),
(16, 1, 1, 1, 'bedroom', '', 'free'),
(17, 1, 1, 1, 'bedroom', '', 'free'),
(18, 1, 8, 8, 'bedroom', '', 'free'),
(19, 1, 14, 14, 'bedroom', '', 'free'),
(20, 2, 3, 3, 'bedroom', '', 'free'),
(21, 2, 1, 1, 'bedroom', '', 'free'),
(22, 2, 1, 1, 'bedroom', '', 'free'),
(24, 2, 4, 4, 'bedroom', '', 'free'),
(25, 2, 1, 1, 'bedroom', '', 'free'),
(26, 2, 1, 1, 'bedroom', '', 'free'),
(27, 2, 1, 1, 'bedroom', '', 'free'),
(28, 2, 8, 8, 'bedroom', '', 'free'),
(29, 2, 14, 14, 'bedroom', '', 'free'),
(30, 3, 3, 3, 'bedroom', '', 'free'),
(31, 3, 1, 1, 'bedroom', '', 'free'),
(32, 3, 1, 1, 'bedroom', '', 'free'),
(33, 3, 1, 1, 'bedroom', '', 'free'),
(35, 3, 4, 4, 'bedroom', '', 'free'),
(36, 3, 1, 1, 'bedroom', '', 'free'),
(37, 3, 1, 1, 'bedroom', '', 'free'),
(38, 3, 8, 8, 'bedroom', '', 'free'),
(39, 3, 14, 14, 'bedroom', '', 'free'),
(100, 0, 0, 0, 'bathroom', 'Bathroom', 'free'),
(101, 1, 0, 0, 'bathroom', 'Bathroom', 'free'),
(102, 2, 0, 0, 'bathroom', 'Bathroom', 'free'),
(103, 3, 0, 0, 'bathroom', 'Bathroom', 'closed'); 

  
  
--
-- Dumping data for table `tbl_participants`
--
  
  
CREATE TABLE `tbl_participants` (
  `userid` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `emailAddress` varchar(255) NOT NULL,
  `sharesRoom` varchar(3) NOT NULL,
  `foodPreference` varchar(50) NOT NULL,
  `roomNo` int(3) NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tbl_participants` ADD PRIMARY KEY (`userid`);
ALTER TABLE `tbl_participants` MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT;


--
-- Dumping data for table `tbl_travel`
--
  
  
CREATE TABLE `tbl_travel` (
  `userid` int(11) UNIQUE NOT NULL,
  `arrivalDate` varchar(255) NOT NULL,
  `departureDate` varchar(255) NOT NULL,
  `arrivalAirport` varchar(255) NULL,
  `departureAirport` varchar(255) NULL,
  `travelCost` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Dumping data for table `tbl_prices`
--
  
  
CREATE TABLE `tbl_prices` (
  `article` varchar(255) NOT NULL,
  `price` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_prices` (`article`, `price`) VALUES
('bedroomPerNight', 15.50),
('sharedroomPerNight', 12.00),
('bedlinen', 30.00),
('beer', 2.50),
('foodPerDay', 15.00);


--
-- Dumping data for table `tbl_projects`
--
  
  
CREATE TABLE `tbl_projects` (
  `name` varchar(255) NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tbl_projects` ADD PRIMARY KEY (`name`);


--
-- Dumping data for table `tbl_projects`
--
  
  
CREATE TABLE `tbl_users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`), UNIQUE (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
