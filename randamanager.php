        <?php
            include("header.php");
            /*$csvFile = "Randa Meetings 2016.csv";
            
            if (file_exists($csvFile)) {
                readfile($csvFile);
            }*/
            

            ///////////////////////////////////////////////
            // Show free rooms
            ////////////////////
//             echo "<h1>Free single bedrooms</h1>";
//             $sql = "SELECT * FROM tbl_rooms WHERE bedNo = 1 AND roomStatus = 'free'";
//             $rooms = $pdo->query($sql);
//             
//             while ($room = $rooms->fetch_assoc()) {
//                 echo "Room #: " . $room['roomNo'] . "<br />";
//             }
//             
//             echo "<h1>Free shared bedrooms</h1>";
//             $sql = "SELECT * FROM tbl_rooms WHERE bedNo > 4 AND (roomStatus = 'free' OR roomStatus = 'halffree')";
//             $rooms = $pdo->query($sql);
//             
//             while ($room = $rooms->fetch_assoc()) {
//                 echo "Room #: " . $room['roomNo'] . "<br />";
//             }
//             
//             echo "<h1>Free other bedrooms</h1>";
//             $sql = "SELECT * FROM tbl_rooms WHERE bedNo > 1 AND bedNo < 8 AND (roomStatus = 'free' OR roomStatus = 'halffree')";
//             $rooms = $pdo->query($sql);
//             
//             while ($room = $rooms->fetch_assoc()) {
//                 echo "Room #: " . $room['roomNo'] . "<br />";
//             }
            
            ///////////////////////////////////////////////
            // Calculate costs per participant
            ////////////////////
            $pStatement = $pdo->prepare("SELECT * FROM tbl_participants;");
            $pStatement->execute();
            
            if ($pStatement->rowCount() > 0) {
                while($participant = $pStatement->fetch()) {
                    $userid = $participant['userid'];

                    echo "$userid Name: " . $participant['firstname'] . " ";
                    
                    $dStatement = $pdo->prepare("SELECT arrivalDate, departureDate FROM tbl_travel WHERE userid = ?");
                    $dStatement->execute(array($userid));
                    $travelData = $dStatement->fetch();
                    $arrivalDate = strtotime($travelData['arrivalDate']);
                    $departureDate = strtotime($travelData['departureDate']);
                    $noOfNights = ($departureDate - $arrivalDate) / 86400;
                    $noOfDays = $noOfNights + 1;
                    
                    $roomNo = $participant['roomNo'];
                    
                    $rStatement = $pdo->prepare("SELECT roomType FROM tbl_rooms WHERE roomNo = ?");
                    $rStatement->execute(array($roomNo));
                    // FIXME: Check if there is a result at all
                    $roomType = $rStatement->fetch()['roomType'];
                    
                    if ($roomType == "sharedroom") {
                        $rpStatement = $pdo->prepare("SELECT price FROM tbl_prices WHERE article = 'sharedroomPerNight'");
                    } elseif ($roomType == "bedroom") {
                        $rpStatement = $pdo->prepare("SELECT price FROM tbl_prices WHERE article = 'bedroomPerNight'");
                    } else {
                        // FIXME: Handle this better
                        die("Problem!");
                    }
                    $rpStatement->execute();
                    
                    // FIXME: Check if there is a result at all
                    $roomPrice = $rpStatement->fetch()['price'];

                    $fpStatement = $pdo->prepare("SELECT price FROM tbl_prices WHERE article = 'foodPerDay'");
                    $fpStatement->execute();
                    $foodPrice = $fpStatement->fetch()['price'];
                    
                    $accommodationCost = $roomPrice * $noOfNights;
                    $foodCost = $foodPrice * $noOfDays;
                    
                    echo $roomType . " " . $accommodationCost . " " . $foodCost;
                    echo "<br />";
                    
                    
                    //                     $sql = "SELECT * FROM tbl_rooms WHERE bedNo > 4 AND freeBeds > 0";
//                     $sharedRoom = $pdo->query($sql);
//                     
//                     if ($sharedRoom->num_rows > 0) {
//                         $room = $sharedRoom->fetch_assoc();
//                         $roomNo = $room['roomNo'];
//                         $freeBeds = $room['freeBeds'];
//                         $freeBeds--;
//                         $userid = $participant['userid'];
//                         echo "Room #: " . $roomNo . "<br />";
//                         if ($freeBeds == 0) {
//                             $sql = "UPDATE tbl_rooms SET roomStatus = 'reserved', freeBeds = '$freeBeds'  WHERE roomNo = '$roomNo'";                        
//                         } else {
//                             $sql = "UPDATE tbl_rooms SET roomStatus = 'halffree', freeBeds = '$freeBeds'  WHERE roomNo = '$roomNo'";                        
//                         }
//                         $pdo->query($sql);
//                         $sql = "UPDATE tbl_participants SET roomNo = '$roomNo' WHERE userid = '$userid'";
//                         $pdo->query($sql);
//                     }
//                     else {
//                         echo "There is no group room for " . $participant['firstname'] . " " . $participant['lastname'] . ". ";
//                         echo "Please add this person manually to a room.<br />";
//                     }

                }
            } else {
                echo "0 participants<br />";
            }            
            
            // TODO: Add possibility to add paid money, change dates of participants, etc.
            // TODO: Create balance at the end of the meetings
            // TODO: Create pdf bill
            // TODO: Create all signs for a year with correct year, wiki link, etc.
            include("footer.php");
        ?>
