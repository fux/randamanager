<?php
    include("header.php");
    
    if (!isset($_SESSION['id'])) {
        die('Bitte zuerst <a href="login.php">einloggen</a></body></html>');
    }
    
    // Debug line
//     $_SESSION['projectStarted'] = "no";
        
    if ($_SESSION['projectStarted'] != "yes") {
        echo "<h3>Projekt starten</h3>";
        echo "<form action='#' method='post' enctype='multipart/form-data'>";
        echo "<input type='file' name='uploadedFile' />";
        echo "<input type='submit' name='csvImportButton' value='CSV importieren'/>";
        echo "</form>";
    } else {
        $_SESSION['projectName'] = "Randa Meetings 2017";
//         $_SESSION['projectStarted'] = "";
        // Read projectName from db
        $projectName = $_SESSION['projectName'];
        
        $projectStatement = $pdo->prepare("SELECT startDate, endDate FROM tbl_projects WHERE name = ?");
        $projectStatement->execute(array($projectName));
                
        if ($projectStatement->rowCount() == 1) {
            $projectDates = $projectStatement->fetch();
        } else {
            die("<p>No project found with name $projectName</p>");
        }
        
        
        echo "<h3>Project <i>" . $projectName . "</i> ist geöffnet</h3>";
        
        // TODO: Calculate how many days are left till project/meeting begin
        $daysLeft = 200;
        $startDate = date_create($projectDates['startDate']);
        $endDate = date_create(date("d.m.Y", time()));
        $interval = date_diff($startDate, $endDate);
        echo "<p>T: " . $interval->format('%R%a Tage') . "</p>";
        
    }
    
    if (isset($_POST['csvImportButton'])) {

        // FIXME: Test if the file is really a csv-file and no bogus
        $filename = "data/test.csv";
//         echo "Datei: "  . " More: " . pathinfo($_FILES["uploadedFile"]["name"],PATHINFO_BASENAME) . basename( $_FILES["uploadedFile"]["name"]);
        move_uploaded_file($_FILES["uploadedFile"]["tmp_name"], $filename);
        
        
        //////////////////////////////////////////
        // Read participants list and insert into db
        $i = 1;
        $buffer;
        $handle = @fopen($filename, "r");
        if ($handle) {
            while (($buffer = fgets($handle)) !== false) {
                echo "<i>" . $i . "</i>   " . $buffer . "<br />";
                $lineArray = explode(",", $buffer);
                $name = $lineArray[0];
                $firstname = explode(" ", $lineArray[0])[0];
                $lastname = trim(str_replace($firstname, "", $name));
                $emailAddress = $lineArray[1];
                $sharesRoom = $lineArray[8];
                $foodPreference = $lineArray[9];
                $statement = $pdo->prepare("INSERT INTO tbl_participants
                            (userid, firstname, lastname, emailAddress, sharesRoom, foodPreference)
                            VALUES (:userid, :firstname, :lastname, :emailAddress, :sharesRoom, :foodPreference)");
                $statement->execute(array('userid' => $i, 'firstname' => $firstname, 'lastname' => $lastname,
                                          'emailAddress' => $emailAddress, 'sharesRoom' => $sharesRoom, 'foodPreference' => $foodPreference));
                
                $arrivalDate = $lineArray[6];
                $departureDate = $lineArray[7];
                $travelCost = $lineArray[4];
                
                $statement = $pdo->prepare("INSERT INTO tbl_travel 
                        (userid, arrivalDate, departureDate, arrivalAirport, departureAirport, travelCost)
                        VALUES (:userid, :arrivalDate, :departureDate, '', '', :travelCost)");
                $statement->execute(array('userid' => $i, 'arrivalDate' => $arrivalDate, 'departureDate' => $departureDate,
                                          'travelCost' => $travelCost));
                $i++;
            }
            if (!feof($handle)) {
                echo "Fehler: unerwarteter fgets() Fehlschlag\n";
            }
            fclose($handle);
        }
        
        //////////////////////////////////////////
        // Read participants list and insert into db
        
        $projectName = "Randa Meetings 2017";
        
        $statement = $pdo->prepare("INSERT INTO tbl_projects VALUES(?, '2017-09-10', '2017-09-16'");
        $statement->execute(array($projectName));
        
        $_SESSION['projectStarted'] = "yes";
        $_SESSION['projectName'] = "$projectName";
        
        echo $buffer;
    } else {
//         echo "Noch keine Datei ausgewählt.";
    }
    
    
    
    
    ///////////////////////////////////////////////
    // Room distribution
    ////////////////////   
    echo "<h3>Zimmer verteilen</h3>";
    echo "<form action='#' method='post'>";
    echo "<input type='submit' name='distributeRoomsButton' value='Teile Zimmer TeilnehmerInnen zu'/>";
    echo "</form>";
    
    if (isset($_POST['distributeRoomsButton'])) {
        echo "Zimmer verteilen...<br />";

        // first the single rooms
        $sql = "SELECT * from tbl_participants WHERE sharesRoom = 'no'";

        $singleroomparticipants = $pdo->query($sql);
        
        if ($singleroomparticipants->rowCount() > 0) {
            while($participant = $singleroomparticipants->fetch()) {
                $sql = "SELECT * FROM tbl_rooms WHERE bedNo = 1 AND roomStatus = 'free'";
                $singleroom = $pdo->query($sql);
                
                if ($singleroom->rowCount() > 0) {
                    $room = $singleroom->fetch();
                    $roomNo = $room['roomNo'];
                    $userid = $participant['userid'];
                    echo $participant['firstname'] . " is in room #: " . $roomNo . "<br />";
                    $statement = $pdo->prepare("UPDATE tbl_rooms SET roomstatus = 'reserved', freeBeds = 0 WHERE roomNo = ?");
                    $statement->execute(array($roomNo));
                    $statement = $pdo->prepare("UPDATE tbl_participants SET roomNo = :roomNo WHERE userid = :userid");
                    $statement->execute(array("roomNo" => $roomNo, "userid" => $userid));
                }
                else {
                    echo "there is no single room for " . $participant['firstname'] . " " . $participant['lastname'] . ". ";
                    echo "please add this person manually to a room.<br />";
                }

            }
        } else {
            echo "0 results";
        }
            
        // And now the grouprooms
        $sql = "SELECT * FROM tbl_participants WHERE sharesRoom = 'Yes'";

        $sharedRoomParticipants = $pdo->query($sql);
        
        if ($sharedRoomParticipants->rowCount() > 0) {
            while($participant = $sharedRoomParticipants->fetch()) {
//                 echo "Name #: " . $participant['firstname'] . "<br />";
                $sql = "SELECT * FROM tbl_rooms WHERE bedNo > 4 AND freeBeds > 0";
                $sharedRoom = $pdo->query($sql);
                
                if ($sharedRoom->rowCount() > 0) {
                    $room = $sharedRoom->fetch();
                    $roomNo = $room['roomNo'];
                    $freeBeds = $room['freeBeds'];
                    $freeBeds--;
                    $userid = $participant['userid'];
                    echo $participant['firstname'] . " is in room #: " . $roomNo . "<br />";
                    if ($freeBeds == 0) {
                        $statement = $pdo->prepare("UPDATE tbl_rooms SET roomStatus = 'reserved', freeBeds = :freeBeds  WHERE roomNo = :roomNo");                        
                    } else {
                        $statement = $pdo->prepare("UPDATE tbl_rooms SET roomStatus = 'halffree', freeBeds = :freeBeds  WHERE roomNo = :roomNo");                        
                    }
                    $statement->execute(array("freeBeds" => $freeBeds, "roomNo" => $roomNo));
                    $statement = $pdo->prepare("UPDATE tbl_participants SET roomNo = :roomNo WHERE userid = :userid");
                    $statement->execute(array("roomNo" => $roomNo, "userid" => $userid));
                }
                else {
                    echo "There is no group room for " . $participant['firstname'] . " " . $participant['lastname'] . ". ";
                    echo "Please add this person manually to a room.<br />";
                }

            }
        } else {
            echo "0 results";
        }

    }
    
    ///////////////////////////////////////////////
    // Create Accommodation table
    ///////////////////////////////////////////////
    echo "<h3>Übernachtungstabelle erzeugen</h3>";

    if (isset($_POST['createAccommodationTable'])) {
        $statement = $pdo->prepare("SELECT startDate, endDate FROM tbl_projects WHERE name = ?");
        $statement->execute(array($projectName));
        if ($statement->rowCount() == 1) {
            $projectDates = $statement->fetch();
        } else {
            die("<p>No project found with name $projectName</p>");
        }
        
        // FIXME: Must be easier to accomplish ;-)
        $startString = $projectDates['startDate'];
        $endString = $projectDates['endDate'];
        
        // FIXME: Tricky part "-"-separator, depends on locale, please fix and change and fix
        $startArray = explode("-", $startString);
        $endArray = explode("-", $endString);
        
        $startTime = mktime(0, 0, 0, $startArray[1], $startArray[2], $startArray[0]);
        $endTime = mktime(0, 0, 0, $endArray[1], $endArray[2], $endArray[0]);
        
        $startDate = getDate($startTime);
        $endDate = getDate($endTime);
        
        $days = date_diff(date_create($startString), date_create($endString));
        $dayNo = intval($days->format('%a'));
        
        // TODO: Change varchar in tbl_travel to date and see what breaks ;-)

        $accommodationTableName = "tbl_accommodation";
        
        $createSql = "CREATE TABLE $accommodationTableName (";
        // TODO: Make a foreigh key attribute with userid
        $createSql .= " userid INT UNSIGNED NOT NULL PRIMARY KEY, ";
        
        $currentDate = getDate($startTime);
        for ($i = 0; $i <= $dayNo; $i++) {
            $currentDate = getDate($startTime + $i*86400);
            $dayNoString = $i + 1;
            if ($i < $dayNo) {
                $createSql .= " day$dayNoString VARCHAR(10) NOT NULL,";
            } else {
                $createSql .= " day$dayNoString VARCHAR(10) NOT NULL";
            }
        }
        
        $createSql .= ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        
        $accommodationTableCreated = true;
        if ($pdo->query($createSql)) {
            echo "<p>Creating of $accommodationTableName successful.</p>";
        } else {
            $accommodationTableCreated = false;
        }
        
        $sql = "SELECT tbl_participants.userid, firstname, lastname, arrivalDate, departureDate FROM tbl_participants INNER JOIN tbl_travel ON tbl_participants.userid = tbl_travel.userid";
        $participants = $pdo->query($sql);
        
        if ($participants->rowCount() > 0) {
            while ($participant = $participants->fetch()) {
                $userid = $participant['userid'];
                $createUserEntrySql = "INSERT INTO $accommodationTableName VALUES($userid, ";
                
                $arrivalString =  $participant['arrivalDate'];
                $arrivalDate = date_create($arrivalString);
                $departureString = $participant['departureDate'];
                $departureDate = date_create($departureString);
                
                for ($i = 0; $i <= $dayNo; $i++) {
                    $currentDateArray = getDate($startTime + $i*86400);
                    $currentDate = date_create($currentDateArray['year'] . "-" . $currentDateArray['mon'] . "-" . $currentDateArray['mday']);
                    if ($arrivalDate < $currentDate AND $departureDate > $currentDate) {
                        $createUserEntrySql .= "\"There\", ";
                    } elseif ($arrivalDate == $currentDate) {
                        $createUserEntrySql .= "\"Arriving\", ";
                    } elseif ($departureDate == $currentDate) {
                        $createUserEntrySql .= "\"Departing\", ";
                    } else{
                        $createUserEntrySql .= "\"Not there\", ";
                    }
                }
                
                // Remove the last comma
                $createUserEntrySql = substr($createUserEntrySql, 0, strlen($createUserEntrySql)-2);
                $createUserEntrySql .= ");";
                if ($pdo->query($createUserEntrySql)) {
                    echo "<p>Added user with ID $userid to $accommodationTableName.</p>";
                    $accommodationTableCreated = true;
                } else {
                    echo "<p>$createUserEntrySql</p>";
                }
            }
        }
        
        if ($accommodationTableCreated) {
            echo "<p>Übernachtungstabelle erzeugt!</p>";
        } else {
            echo "<p>Some problems occurred during creating of $accommodationTableName.</p>";
        }
    } else {
        echo "<form action='#' method='post'>";
        echo "<input type='submit' name='createAccommodationTable' value='Erzeuge Übernachtungstabelle'/>";
        echo "</form>";
    }


    ///////////////////////////////////////////////
    // Create Food table
    ///////////////////////////////////////////////
    echo "<h3>Mahlzeitentabelle erzeugen</h3>";

    if (isset($_POST['createFoodTable'])) {
        $statement = $pdo->prepare("SELECT startDate, endDate FROM tbl_projects WHERE name = ?");
        $statement->execute(array($projectName));
        if ($statement->rowCount() == 1) {
            $projectDates = $statement->fetch();
        } else {
            die("<p>No project found with name $projectName</p>");
        }
        
        // FIXME: Must be easier to accomplish ;-)
        $startString = $projectDates['startDate'];
        $endString = $projectDates['endDate'];
        
        // FIXME: Tricky part "-"-separator, depends on locale, please fix and change and fix
        $startArray = explode("-", $startString);
        $endArray = explode("-", $endString);
        
        $startTime = mktime(0, 0, 0, $startArray[1], $startArray[2], $startArray[0]);
        $endTime = mktime(0, 0, 0, $endArray[1], $endArray[2], $endArray[0]);
        
        $startDate = getDate($startTime);
        $endDate = getDate($endTime);
        
        $days = date_diff(date_create($startString), date_create($endString));
        $dayNo = intval($days->format('%a'));
        
        // TODO: Change varchar in tbl_travel to date and see what breaks ;-)

        $foodTableName = "tbl_meals";
        
        $createSql = "CREATE TABLE $foodTableName (";
        // TODO: Make a foreigh key attribute with userid
        $createSql .= " userid INT UNSIGNED NOT NULL PRIMARY KEY, ";
        
        $currentDate = getDate($startTime);
        for ($i = 0; $i <= $dayNo; $i++) {
            $currentDate = getDate($startTime + $i*86400);
            $dayNoString = $i + 1;
            if ($i < $dayNo) {
                $createSql .= " day$dayNoString" . "_B VARCHAR(1) NOT NULL,";
                $createSql .= " day$dayNoString" . "_L VARCHAR(1) NOT NULL,";
                $createSql .= " day$dayNoString" . "_D VARCHAR(1) NOT NULL,";
            } else {
                $createSql .= " day$dayNoString" . "_B VARCHAR(1) NOT NULL,";
                $createSql .= " day$dayNoString" . "_L VARCHAR(1) NOT NULL,";
                $createSql .= " day$dayNoString" . "_D VARCHAR(1) NOT NULL";
            }
        }
        
        $createSql .= ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        
        $foodTableCreated = true;
        if ($pdo->query($createSql)) {
            echo "<p>Creating of $foodTableName successful.</p>";
        } else {
            $foodTableCreated = false;
        }
        
        $sql = "SELECT tbl_participants.userid, foodPreference, arrivalDate, departureDate FROM tbl_participants INNER JOIN tbl_travel ON tbl_participants.userid = tbl_travel.userid";
        $participants = $pdo->query($sql);
        
        if ($participants->rowCount() > 0) {
            while ($participant = $participants->fetch()) {
                $userid = $participant['userid'];
                $createUserEntrySql = "INSERT INTO $foodTableName VALUES($userid, ";
                
                $arrivalString =  $participant['arrivalDate'];
                $arrivalDate = date_create($arrivalString);
                $departureString = $participant['departureDate'];
                $departureDate = date_create($departureString);
                
                // Create foodPreferenceEntry for tbl_meals
                $foodPreference = $participant['foodPreference'];
                $foodPreference = trim($foodPreference);
                
                $foodPreferenceString;
                if ($foodPreference == "I eat everything") {
                    $foodPreferenceString = "C";
                } elseif($foodPreference == "Vegetarian") {
                    $foodPreferenceString = "V";
                } else {
                    $foodPreferenceString = "U";
                }
                
                for ($i = 0; $i <= $dayNo; $i++) {
                    $currentDateArray = getDate($startTime + $i*86400);
                    $currentDate = date_create($currentDateArray['year'] . "-" . $currentDateArray['mon'] . "-" . $currentDateArray['mday']);
                    if ($arrivalDate < $currentDate AND $departureDate > $currentDate) {
//                         $createUserEntrySql .= "\"There\", ";
                        $createUserEntrySql .= "\"$foodPreferenceString\", \"$foodPreferenceString\", \"$foodPreferenceString\",";
                    } elseif ($arrivalDate == $currentDate) {
//                         $createUserEntrySql .= "\"Arriving\", ";
                        $createUserEntrySql .= "\"X\", \"$foodPreferenceString\", \"$foodPreferenceString\",";
                    } elseif ($departureDate == $currentDate) {
//                         $createUserEntrySql .= "\"Departing\", ";
                        $createUserEntrySql .= "\"$foodPreferenceString\", \"X\", \"X\",";
                    } else{
//                         $createUserEntrySql .= "\"Not there\", ";
                        $createUserEntrySql .= "\"X\", \"X\", \"X\",";
                    }
                }
                
                // Remove the last comma
                $createUserEntrySql = substr($createUserEntrySql, 0, strlen($createUserEntrySql)-1);
                $createUserEntrySql .= ");";

                if ($pdo->query($createUserEntrySql)) {
//                     echo "<p>Added user with ID $userid to $foodTableName.</p>";
                    $foodTableCreated = true;
                } else {
                    echo "<p>$createUserEntrySql</p>";
                }
            }
        }
        
        if ($foodTableCreated) {
            echo "<p>Mahlzeitentabelle erzeugt!</p>";
        } else {
            echo "<p>Some problems occurred during creating of $foodTableName.</p>";
        }
    } else {
        echo "<form action='#' method='post'>";
        echo "<input type='submit' name='createFoodTable' value='Erzeuge Mahlzeitentabelle'/>";
        echo "</form>";
    }
    
    include("footer.php");
?>
