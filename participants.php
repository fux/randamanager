<?php
    include("header.php");
    include("utils.php");
    
    if (!isset($_SESSION['id'])) {
        die('Bitte zuerst <a href="login.php">einloggen</a></body></html>');
    }
    
    // Debug switch
    $_SESSION['projectStarted'] = "yes";
    
    if (isset($_SESSION['projectStarted']) && $_SESSION['projectStarted'] == "yes") {
        echo "<p>Welcome to the project ... </p>";
        
        //////////////////////////////////////////////////////////////////////////////
        // Detail and Edit modes
        if (isset($_GET['userid'])) {
            $userid = $_GET['userid'];
            $_SESSION['userid'] = $userid;
            //////////////////////////////////////////////////////////////////////////////
            // Details mode
            if (isset($_GET['mode']) && $_GET['mode'] == "details") {
                echo "<h1>Details</h1>";
                
                $pStatement = $pdo->prepare("SELECT * from tbl_participants WHERE userid = ?");
                $pStatement->execute(array($userid));
                
                $tStatement = $pdo->prepare("SELECT * from tbl_travel WHERE userid = ?");
                $travels = $tStatement->execute(array($userid));
                
                if ($pStatement->rowCount() == 1 AND $tStatement->rowCount() == 1) {
                    $participant = $pStatement->fetch();
                    $travelData = $tStatement->fetch();
                 
                    $firstname = $participant['firstname'];
                    $lastname = $participant['lastname'];
                    $emailAddress = $participant['emailAddress'];
                    $sharesRoom = $participant['sharesRoom'];
                    $foodPreference = $participant['foodPreference'];
                    $roomNo = $participant['roomNo'];
                    
                    $arrivalDate = $travelData['arrivalDate'];
                    $departureDate = $travelData['departureDate']; 
                    $arrivalAirport = $travelData['arrivalAirport']; 
                    $departureAirport = $travelData['departureAirport']; 
                    $travelCost = $travelData['travelCost'];
                    
                    echo "<table>";
                    echo "<tr><td><b>Vorname:</b></td><td>$firstname</td><tr>";
                    echo "<tr><td><b>Nachname:</b></td><td>$lastname</td><tr>";
                    echo "<tr><td><b>E-Mail-Adresse:</b></td><td><a href='mailto:$emailAddress'>$emailAddress</a></td><tr>";
                    echo "<tr><td><b>Teilt Raum:</b></td><td>$sharesRoom</td><tr>";
                    echo "<tr><td><b>Essensvorliebe:</b></td><td>$foodPreference</td><tr>";
                    echo "<tr><td><b>Raum-Nummer:</b></td><td>$roomNo</td><tr>";
                    echo "<tr><td><b>Anreisetag:</b></td><td>$arrivalDate</td><tr>";
                    echo "<tr><td><b>Abreisetag:</b></td><td>$departureDate</td><tr>";
                    echo "<tr><td><b>Anreise-Flughafen:</b></td><td>$arrivalAirport</td><tr>";
                    echo "<tr><td><b>Abreise-Flughafen:</b></td><td>$departureAirport</td><tr>";
                    echo "<tr><td><b>Reisekosten:</b></td><td>$travelCost</td><tr>";
                    echo "</table>";
                    echo "<p><a href='participants.php?userid=$userid&mode=edit'>Edit</a></p>";
                    createParticipantPdf($userid, $firstname, $lastname);
                }
                
                echo "<p><a href='participants.php'>Zur&uuml;ck</a></p>";
            }
            //////////////////////////////////////////////////////////////////////////////
            // Edit mode
            elseif (isset($_GET['mode']) && $_GET['mode'] == "edit") {
                if (isset($_GET['userid']) && !isset($_POST['submit'])) {
                    echo "<h1>Details</h1>";
                    $userid = $_GET['userid'];
                    
                    if ($userid == 0) {
                        echo "<form action='' method='POST'>";
                        echo "<input type='hidden' name='userid' value='$userid' />";
                        echo "<input type='hidden' name='oldRoomNo' value='' />";
                        echo "<table>";
                        echo "<tr><td>Vorname:</td><td><input name='firstname' value=''></td></tr>";
                        echo "<tr><td>Nachname:</td><td><input name='lastname' value=''></td></tr>";
                        echo "<tr><td>E-Mail-Adresse:</td><td><input name='emailAddress' value=''></td></tr>";
                        echo "<tr><td>Teilt Raum:</td><td><input name='sharesRoom' value=''></td><tr>";
                        echo "<tr><td>Essensvorliebe:</td><td><input name='foodPreference' value=''></td><tr>";
                        echo "<tr><td>Raum-Nummer:</td><td><input name='roomNo' value=''></td><tr>";
                        echo "<tr><td>Anreisetag:</td><td><input name='arrivalDate' value='yyyy/mm/dd'></td><tr>";
                        echo "<tr><td>Abreisetag:</td><td><input name='departureDate' value='yyyy/mm/dd'></td><tr>";
                        echo "<tr><td>Anreise-Flughafen:</td><td><input name='arrivalAirport' value=''></td><tr>";
                        echo "<tr><td>Abreise-Flughafen:</td><td><input name='departureAirport' value=''></td><tr>";                                          echo "<tr><td>Reisekosten:</td><td><input name='travelCost' value=''></td><tr>";
                        echo "</table>";
                        echo "<input type='submit' name='submit' value='Neuen Datensatz hinzufügen' />";
                        echo "</form>";
                    } else {
                        $pStatement = $pdo->prepare("SELECT * from tbl_participants WHERE userid = ?");
                        $pStatement->execute(array($userid));
                
                        $tStatement = $pdo->prepare("SELECT * from tbl_travel WHERE userid = ?");
                        $travels = $tStatement->execute(array($userid));
                    }
                
                    if ($userid != 0 AND $pStatement->rowCount() == 1 AND $tStatement->rowCount() == 1) {
                        $participant = $pStatement->fetch();
                        $participantTravelData = $tStatement->fetch();
                        
                        $firstname = $participant['firstname'];
                        $lastname = $participant['lastname'];
                        $emailAddress = $participant['emailAddress'];
                        $sharesRoom = $participant['sharesRoom'];
                        $foodPreference = $participant['foodPreference'];
                        if (isset($_GET['newRoomNo'])) {
                            $roomNo = $_GET['newRoomNo'];
                        } else {
                            $roomNo = $participant['roomNo'];
                        }
                        $arrivalDate = $participantTravelData['arrivalDate'];
                        $departureDate = $participantTravelData['departureDate'];
                        $arrivalAirport = $participantTravelData['arrivalAirport'];
                        $departureAirport = $participantTravelData['departureAirport'];
                        $travelCost = $participantTravelData['travelCost'];
                        
                        echo "<form action='' method='POST'>";
                        echo "<input type='hidden' name='userid' value='$userid' />";
                        echo "<input type='hidden' name='oldRoomNo' value='$roomNo' />";
                        echo "<table>";
                        echo "<tr><td>Vorname:</td><td><input name='firstname' value='$firstname'></td></tr>";
                        echo "<tr><td>Nachname:</td><td><input name='lastname' value='$lastname'></td></tr>";
                        echo "<tr><td>E-Mail-Adresse:</td><td><input name='emailAddress' value='$emailAddress'></td></tr>";
                        echo "<tr><td>Teilt Raum:</td><td><input name='sharesRoom' value='$sharesRoom'></td><tr>";
                        echo "<tr><td>Essensvorliebe:</td><td><input name='foodPreference' value='$foodPreference'></td><tr>";
                        echo "<tr><td>Raum-Nummer:</td><td><input name='roomNo' value='$roomNo'></td><tr>";
                        echo "<tr><td>Anreisetag:</td><td><input name='arrivalDate' value='$arrivalDate'></td><tr>";
                        echo "<tr><td>Abreisetag:</td><td><input name='departureDate' value='$departureDate'></td><tr>";
                        echo "<tr><td>Anreise-Flughafen:</td><td><input name='arrivalAirport' value='$arrivalAirport'></td><tr>";
                        echo "<tr><td>Abreise-Flughafen:</td><td><input name='departureAirport' value='$departureAirport'></td><tr>";
                        echo "<tr><td>Reisekosten:</td><td><input name='travelCost' value='$travelCost'></td><tr>";
                        echo "</table>";
                        echo "<input type='submit' name='submit' value='Daten ändern' />";
                        echo "</form>";
                    }
                    
                // Data is actually edited now
                } elseif (isset($_POST['submit'])) { 
                    $userid = $_POST['userid'];
                    $firstname = $_POST['firstname'];
                    $lastname = $_POST['lastname'];
                    $emailAddress = $_POST['emailAddress'];
                    $sharesRoom = $_POST['sharesRoom'];
                    $foodPreference = $_POST['foodPreference'];
                    $newRoomNo = $_POST['roomNo'];
                    $oldRoomNo = $_POST['oldRoomNo'];
                    
                    $arrivalDate = $_POST['arrivalDate'];
                    $departureDate = $_POST['departureDate'];
                    $arrivalAirport = $_POST['arrivalAirport'];
                    $departureAirport = $_POST['departureAirport'];
                    $travelCost = $_POST['travelCost'];

                    /////////////////////////////////////////////////////
                    // Date checks
                    // FIXME: as soon as we get correct date handling
                    $arrivalDateArray = explode("/", $arrivalDate);
                    $departureDateArray = explode("/", $departureDate);
                    
                    if (checkdate($arrivalDateArray[1], $arrivalDateArray[2], $arrivalDateArray[0])) {
                        echo "<p>ArrivalDate is correct</p>";
                    } else {
                        die("<p>ArrivalDate is NOT correct: $arrivalDate </p>");
                    }
                    
                    if (checkdate($departureDateArray[1], $departureDateArray[2], $departureDateArray[0])) {
                        echo "<p>DepartureDate is correct</p>";
                    } else {
                        die("<p>DepartureDate is NOT correct: $departureDate </p>");
                    }
                    
                
                    // TODO: Check if old data is new data and don't do anything then
                    // TODO: Check if dates are ok and recreate Accommodation table and Food table
                    // TODO: Check if arrivalDate is before departureDate
                    // TODO: Check if sharesRoom changed and then everything changes (other category of rooms)
                    
                    //////////////////////////////////////////
                    // Adding new participant
                    if ($userid == 0) {
                        echo "<p>Einfügen neuer Daten... nicht vergessen, noch ein passendes Zimmer hinzuzufügen.</p>";
                        
                        // Check the highest userid and increment
                        $result = $pdo->query("SELECT MAX(userid) FROM tbl_participants");
                        $maxUserid = $result->fetch();
                        $newUserid = $maxUserid['MAX(userid)'] + 1;
                        
                        // FIXME: No check (see code below) for availability of the room
                        $statement = $pdo->prepare("INSERT INTO tbl_participants
                            (userid, firstname, lastname, emailAddress, sharesRoom, foodPreference)
                            VALUES (:userid, :firstname, :lastname, :emailAddress, :sharesRoom, :foodPreference)");
                        $statement->execute(array('userid' => $newUserid, 'firstname' => $firstname, 'lastname' => $lastname,
                                            'emailAddress' => $emailAddress, 'sharesRoom' => $sharesRoom, 'foodPreference' => $foodPreference));
                                            
                        $statement = $pdo->prepare("INSERT INTO tbl_travel 
                                                    (userid, arrivalDate, departureDate, arrivalAirport, departureAirport, travelCost)
                                                    VALUES (:userid, :arrivalDate, :departureDate, '', '', :travelCost)");
                        $statement->execute(array('userid' => $newUserid, 'arrivalDate' => $arrivalDate, 'departureDate' => $departureDate,
                                                  'travelCost' => $travelCost));
                    //////////////////////////////////////////
                    // Check the data for the case we need to switch rooms and change if everything is ok
                    // FIXME: Doesn't work too well. Probably if no oldRoomNo is set or so... please check
                    } elseif ($newRoomNo != $oldRoomNo) {
                        // Get data for new room
                        $statement = $pdo->prepare("SELECT roomStatus, bedNo, freeBeds FROM tbl_rooms WHERE roomNo = ?");
                        $statement->execute(array($newRoomNo));
                        
                        if ($statement->rowCount() == 1) {
                            $newRoom = $statement->fetch();
                        }
                        $newRoomStatus = $newRoom['roomStatus'];
                    
                        if ($newRoomStatus == "free" || $newRoomStatus == "halffree") {
                            /////////////////////////////////////////////////////////////////
                            // Change room number for participant
                            
                            $statement = $pdo->prepare("UPDATE tbl_participants SET
                                    firstname = :firstname, 
                                    lastname = :lastname, 
                                    emailAddress = :emailAddress, 
                                    sharesRoom = :sharesRoom, 
                                    foodPreference = :foodPreference, 
                                    roomNo = :newRoomNo 
                                    WHERE userid = :userid");
                            $statement->execute(array("firstname" => $firstname, "lastname" => $lastname, "emailAddress" => $emailAddress,
                                                      "sharesRoom" => $sharesRoom, "foodPreference" => $foodPreference, "newRoomNo" => $newRoomNo,
                                                      "userid" => $userid));
                            $statement = $pdo->prepare("UPDATE tbl_travel SET
                                    arrivalDate = :arrivalDate,
                                    departureDate = :departureDate,
                                    arrivalAirport = :arrivalAirport,
                                    departureAirport = :departureAirport,
                                    travelCost = :travelCost
                                    WHERE userid = :userid");
                            $statement->execute(array("arrivalDate" => $arrivalDate, "departureDate" => $departureDate,
                                                      "arrivalAirport" => $arrivalAirport, "departureAirport" => $departureAirport,
                                                      "travelCost" => $travelCost, "userid" => $userid));
                            // FIXME: recreate Accommodation table!!!
                            
                            $newRoomStatus = "";
                            
                            /////////////////////////////////////////////////////////////////
                            // Change data of old room but first get the data of the old room
                            $statement = $pdo->prepare("SELECT roomStatus, bedNo, freeBeds FROM tbl_rooms WHERE roomNo = ?");
                            $statement->execute(array($oldRoomNo));
                            if ($statement->rowCount() == 1) {
                                $oldRoom = $statement->fetch();
                            }
                            $oldRoomStatus = $oldRoom['roomStatus'];
                            $oldBedNo = $oldRoom['bedNo'];
                            $oldFreeBeds = $oldRoom['freeBeds'];
                            
                            if ($oldBedNo - $oldFreeBeds == 1) {
                                $oldRoomStatus = "free";
                            } else {
                                $oldRoomStatus = "halffree";
                            }
                            
                            $statement = $pdo->prepare("UPDATE tbl_rooms SET freeBeds = freeBeds + 1,
                                                        roomStatus = :oldRoomStatus WHERE roomNo = :oldRoomNo");
                            $statement->execute(array("oldRoomStatus" => $oldRoomStatus, "oldRoomNo" => $oldRoomNo));
                            
                            /////////////////////////////////////////////////////////////////
                            // Change data of new room
                            $newBedNo = $newRoom['bedNo'];
                            $newFreeBeds = $newRoom['freeBeds'];
                            
                            if ($newFreeBeds == 1) {
                                $newRoomStatus = "reserved";
                            } else {
                                $newRoomStatus = "halffree";
                            }
                            $statement = $pdo->prepare("UPDATE tbl_rooms SET freeBeds = freeBeds - 1,
                                                       roomStatus = :newRoomStatus WHERE roomNo = :newRoomNo");
                            $statement->execute(array("newRoomStatus" => $newRoomStatus, "newRoomNo" => $newRoomNo));
                            
                            echo "<p>Daten geändert.</p>";
                        } else {
                        echo "<p>Sorry, der Raum mit der Nummer $newRoomNo besitzt keine freien Betten mehr.</p>";
                            // Show a list of free bedrooms and show the form again, maybe make links and pre-fill the form
                            $sql = "SELECT roomNo, bedNo, freeBeds FROM tbl_rooms WHERE (roomStatus = 'halffree' OR roomStatus = 'free') AND roomType = 'bedroom'";
                            $freeRooms = $pdo->query($sql);
                            echo "<h3>Freie Zimmer</h3>";
                            echo "<table border='1'><tr><th /><th>Zimmernummer</th><th>Anzahl Betten</th><th>Freie Betten</th></tr>";
                            if ($freeRooms->rowCount() > 0) {
                                while($room = $freeRooms->fetch()) {
                                    echo "<tr>";
                                    echo "<td><a href='participants.php?userid=$userid&mode=edit&newRoomNo=" . $room['roomNo'] . "'>Dieses Zimmer für TeilnehmerIn wählen.</a></td>";
                                    echo "<td>" . $room['roomNo'] . "</td>";
                                    echo "<td>" . $room['bedNo'] . "</td>";
                                    echo "<td>" . $room['freeBeds'] . "</td>";
                                    echo "</tr>";
                                }
                            }
                            echo "</table>";
                        }
                    ///////////////////////////////////////////////////////
                    // Change the data for the case the room didn't change
                    } else {
                        $statement = $pdo->prepare("UPDATE tbl_participants SET
                                    firstname = :firstname, 
                                    lastname = :lastname, 
                                    emailAddress = :emailAddress, 
                                    sharesRoom = :sharesRoom, 
                                    foodPreference = :foodPreference, 
                                    roomNo = :newRoomNo 
                                    WHERE userid = :userid");
                        $statement->execute(array("firstname" => $firstname, "lastname" => $lastname, "emailAddress" => $emailAddress,
                                                  "sharesRoom" => $sharesRoom, "foodPreference" => $foodPreference, "newRoomNo" => $newRoomNo,
                                                  "userid" => $userid));
                        $statement = $pdo->prepare("UPDATE tbl_travel SET
                                    arrivalDate = :arrivalDate,
                                    departureDate = :departureDate,
                                    arrivalAirport = :arrivalAirport,
                                    departureAirport = :departureAirport,
                                    travelCost = :travelCost
                                    WHERE userid = :userid");
                        $statement->execute(array("arrivalDate" => $arrivalDate, "departureDate" => $departureDate,
                                                  "arrivalAirport" => $arrivalAirport, "departureAirport" => $departureAirport,
                                                  "travelCost" => $travelCost, "userid" => $userid));
                        // FIXME: recreate Accommodation table!!!
                    }
                }
            echo "<p><a href='participants.php'>Zur&uuml;ck</a></p>";
                
            } else {
                echo "<p>You need to choose a mode.</p>";
            }
            
        }
        
        //////////////////////////////////////////////////////////////////////////////
        // Participations table (all of them)
        else {
            if (isset($_SESSION['userid'])) {
                unset($_SESSION['userid']);
            }
            
            echo "<h3>TeilnehmerInnen</h3>";
                        
            echo "<p><a href='participants.php?userid=0&mode=edit'>TeilnehmerIn hinzuf&uuml;gen</a></p>";
            
            echo "<table border='1'><tr><th>Vorname</th><th>Nachname</th><th /><th /></tr>";
            
            
            // first the single rooms
            $statement = $pdo->prepare("SELECT userid, firstname, lastname FROM tbl_participants");
            $statement->execute();
            
            $participantIndex = 0;
            while($participant = $statement->fetch()) {
                $firstname = $participant['firstname'];
                $lastname = $participant['lastname'];
                $userid = $participant['userid'];
                $participantIndex++;
                if ($participantIndex%2 != 0) {
                    echo "<tr bgcolor='lightgray'>";
                } else {
                    echo "<tr>";
                }
                echo "<td>$firstname</td><td>$lastname</td><td><a href='participants.php?userid=$userid&mode=details'>Details</a></td><td><a href='participants.php?userid=$userid&mode=edit'>Edit</a></td></tr>";
            }
            
            echo "</table>";
        }
        
    } else {
        echo "<p>Please open a <a href=\"projects.php\">new project</a></p>";
    }
    
    
    include("footer.php");
?>
