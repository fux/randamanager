<?php
    function bla() {
        echo "<p><b>BLAAAAAAA<b></p>";
    }
    
    function createParticipantPdf($userid, $firstname, $lastname) {
//         $sql = "SELECT * FROM tbl_participants WHERE userid = $userid";
//         $pdo->query($sql);
        
    
        $billDate = "Randa, " . date("d.m.Y");
        
        $rechnungs_header = '
        <img src="logo.png">
        
        Verein "Randa Meetings"
        c/o Mario Fux
        Hölzliweg 4
        3455 Grünen
        Switzerland
        www.randa-meetings.ch';
        
        $rechnungs_empfaenger = "$firstname $lastname
        Pattern Street 17
        12345 Template-City";
        
        $rechnungs_footer = "Please pay the above amount to the below account in the next 30 days.
        
        <b>Empfänger:</b> Verein \"Randa Meetings\", c/o Mario Fux, Hölzliweg 4, 3400 Switzerland
        <b>IBAN</b>: CH58 908adsf 098asdf08 09saf
        <b>BIC</b>: C46X453AD";
        
        //Auflistung eurer verschiedenen Posten im Format [Produktbezeichnuns, Menge, Einzelpreis]
        $rechnungs_posten = array(
        array("Produkt 1", 1, 42.50),
        array("Produkt 2", 5, 5.20),
        array("Produkt 3", 3, 10.00));
        
        //Höhe eurer Umsatzsteuer. 0.19 für 19% Umsatzsteuer
        $umsatzsteuer = 0.19; 
        
        $pdfName = "Bill_" . $userid . ".pdf";
        
        
        //////////////////////////// Inhalt des PDFs als HTML-Code \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        
        
        // Erstellung des HTML-Codes. Dieser HTML-Code definiert das Aussehen eures PDFs.
        // tcpdf unterstützt recht viele HTML-Befehle. Die Nutzung von CSS ist allerdings
        // stark eingeschränkt.
        
        $html = '
        <table cellpadding="5" cellspacing="0" style="width: 100%; ">
        <tr>
        <td>' . nl2br(trim($rechnungs_header)) . '</td>
            <td style="text-align: right">' .$billDate . '<br>
        </td>
        </tr>
        
        <tr>
        <td style="font-size:1.3em; font-weight: bold;">
        <br><br>
        Receipt
        <br>
        </td>
        </tr>
        
        
        <tr>
        <td colspan="2">' . nl2br(trim($rechnungs_empfaenger)) . '</td>
        </tr>
        </table>
        <br><br><br>';
        
        $html .= "<p><i>Dear $firstname $lastname</i></p>";
        
        $html .= "<p>Thanks for your participation at the Randa Meetings 2017.</p>
        
        <p>You owe us that much money: \$123.00 </p>";
        
   
        $html .= nl2br($rechnungs_footer) . '<p>Best regards<br />Mario Fux<br />
                    <img src="signature.png">';
        
        
        //////////////////////////// Erzeugung eures PDF Dokuments \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        
        // TCPDF Library laden
        require_once('tcpdf/tcpdf.php');
        
        // Erstellung des PDF Dokuments
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        // Dokumenteninformationen
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor("Mario Fux");
        $pdf->SetTitle('Bill ' . $userid);
        $pdf->SetSubject('Bill ' . $userid);
        
        
        // Header und Footer Informationen
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        
        // Auswahl des Font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        
        // Auswahl der Margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        
        // Automatisches Autobreak der Seiten
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        
        // Image Scale 
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        
        // Schriftart
        $pdf->SetFont('dejavusans', '', 10);
        
        // Neue Seite
        $pdf->AddPage();
        
        // Fügt den HTML Code in das PDF Dokument ein
        $pdf->writeHTML($html, true, false, true, false, '');
        
        //Ausgabe der PDF
        
        //Variante 1: PDF direkt an den Benutzer senden:
//          $pdf->Output($pdfName, 'I');
        
        //Variante 2: PDF im Verzeichnis abspeichern:
        $pdf->Output(dirname(__FILE__).'/'.$pdfName, 'F');
        echo 'Quittung herunterladen: <a href="'.$pdfName.'" target="_blank">'.$pdfName.'</a>';
    }
?>
