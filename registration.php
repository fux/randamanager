<?php
    include("header.php");
        
    echo "<h1>Titel</h1>";
    
//     echo "Session there? " . $_SESSION['projectStarted'] . "<br />";
    
    $showFormular = true; //Variable ob das Registrierungsformular anezeigt werden soll
 
    if(isset($_GET['register'])) {
        $error = false;
        $email = $_POST['email'];
        $passwort = $_POST['passwort'];
        $passwort2 = $_POST['passwort2'];
        
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            echo '<p>Bitte eine gültige E-Mail-Adresse eingeben</p>';
            $error = true;
        } 
        if(strlen($passwort) == 0) {
            echo '<p>Bitte ein Passwort angeben</p>';
            $error = true;
        }
        if($passwort != $passwort2) {
            echo '<p>Die Passwörter müssen übereinstimmen</p>';
            $error = true;
        }
        
        //Überprüfe, dass die E-Mail-Adresse noch nicht registriert wurde
        if(!$error) { 
            $statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
            $result = $statement->execute(array('email' => $email));
            $user = $statement->fetch();
            
            if($user !== false) {
                echo '<p>Diese E-Mail-Adresse ist bereits vergeben</p>';
                $error = true;
            } 
        }
        
        //Keine Fehler, wir können den Nutzer registrieren
        if(!$error) { 
            $passwordHash = password_hash($password, PASSWORD_DEFAULT);
            
            $statement = $pdo->prepare("INSERT INTO tbl_users (email, password) VALUES (:email, :password)");
            $result = $statement->execute(array('email' => $email, 'password' => $passwordHash));
            
            if($result) { 
                echo '<p>Du wurdest erfolgreich registriert. <a href="login.php">Zum Login</a></p>';
                $showFormular = false;
            } else {
                echo '<p>Beim Abspeichern ist leider ein Fehler aufgetreten</p>';
            }
        } 
    }
    
    if($showFormular) {
        echo "<h2>Registrierung</h2>";
        echo '<form action="?register=1" method="post">
        E-Mail-Adresse:<br />
        <input type="email" size="40" maxlength="250" name="email"><br />
        
        Dein Passwort:<br />
        <input type="password" size="40"  maxlength="250" name="passwort"><br />
        
        Passwort wiederholen:<br />
        <input type="password" size="40" maxlength="250" name="passwort2"><br /><br />
        
        <input type="submit" value="Abschicken">
        </form>';
    }

    include("footer.php");
?>
