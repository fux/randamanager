<?php
    include("header.php");
    
    
    echo "<h1>Übernachtungen</h1>";
    
    if (!isset($_SESSION['id'])) {
        die('Bitte zuerst <a href="login.php">einloggen</a></body></html>');
    }

    // FIXME: Fix this ;-)
//     $projectName = $_SESSION['projectName'];
    $projectName = "Randa Meetings 2017";

//     $sql = "SELECT startDate, endDate FROM tbl_projects WHERE name = '$projectName'";
//     $result = $pdo->query($sql);
//     if ($result->num_rows == 1) {
//         $projectDates = $result->fetch_assoc();
//     } else {
//         die("<p>No project found with name $projectName</p>");
//     }

    $statement = $pdo->prepare("SELECT startDate, endDate FROM tbl_projects WHERE name = ?");
    $statement->execute(array($projectName));
    // FIXME: Check if there is no result or more than one (see above for old code with if-else)
    $projectDates = $statement->fetch();
    
    // FIXME: Must be easier to accomplish ;-)
    $startString = $projectDates['startDate'];
    $endString = $projectDates['endDate'];
    
    // FIXME: Tricky part "-"-separator, depends on locale, please fix and change and fix
    $startArray = explode("-", $startString);
    $endArray = explode("-", $endString);
    
    $startTime = mktime(0, 0, 0, $startArray[1], $startArray[2], $startArray[0]);
    $endTime = mktime(0, 0, 0, $endArray[1], $endArray[2], $endArray[0]);
    
    $startDate = getDate($startTime);
    $endDate = getDate($endTime);
    
    $days = date_diff(date_create($startString), date_create($endString));
    $dayNo = intval($days->format('%a'));
    
    $accommodationTableName = "tbl_accommodation";
        
    // Participation table
    echo "<table border='1'>";
    
    echo "<tr>";
    echo "<td /><td />";
    $currentDate = getDate($startTime);
    for ($i = 0; $i <= $dayNo; $i++) {
        $currentDate = getDate($startTime + $i*86400);
        $weekday = $currentDate['weekday'];
        echo "<td align='center'>" . $weekday . "</td>";
    }
    echo "</tr>";
    
    echo "<tr>";
    echo "<td>Vorname</td>";
    echo "<td>Nachname</td>";
    $currentTime = $startTime;
    for ($i = 0; $i <= $dayNo; $i++) {
        echo "<td align='center'>" . date('d.m.Y', $currentTime + $i*86400) . "</td>";
    }
    echo "</tr>";
    
    $statement = $pdo->prepare("SELECT * FROM tbl_participants INNER JOIN $accommodationTableName ON tbl_participants.userid = $accommodationTableName.userid");
    $statement->execute();
    
    if ($statement->rowCount() > 0) {
        $participantIndex = 0;
        while ($participant = $statement->fetch()) {
            $participantIndex++;
            if ($participantIndex%2 != 0) {
                echo "<tr bgcolor='lightgray'>";
            } else {
                echo "<tr>";
            }
            $userid = $participant['userid'];
            
            echo "<td><a href='participants.php?userid=$userid&mode=details'>" . $participant['firstname'] . "</a></td>";
            echo "<td><a href='participants.php?userid=$userid&mode=details'>" . $participant['lastname'] . "</a></td>";
            for ($i = 1; $i <= $dayNo+1; $i++) {
                echo "<td align='center'>";
                echo $participant["day$i"];
                echo "</td>";
            }
            echo "</tr>";
        }
    }
    
    //TODO: Add a summary of how many people are sleeping there. For Kur taxes!
    
    
    echo "</table>";    
    
    include("footer.php");
?>
