<?php
    include("header.php");
    
    echo "<br /><br />";
    
    if (isset($_SESSION['userid'])) {
        echo "<p> The user has id " . $_SESSION['userid'] . ".</p>";
    } else {
        echo "<p>No userid set.</p>";
    }
    
    if (!isset($_SESSION['id'])) {
        die('Bitte zuerst <a href="login.php">einloggen</a>');
    }
    
    include("footer.php");
?>
