<!DOCTYPE HTML>
<?php
    session_start();
    include("dbconnect.php");
?>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="style.css" />
        <title>Title</title>
    </head>
    <body>
    <nav id="main_nav">
	<ul>
		<li><a href="index.php" class="current">Home</a></li>
		<li><a href="projects.php">Projects</a></li>
		<li><a href="participants.php">Participants</a></li>
		<li><a href="accommodation.php">Accommodation</a></li>
		<li><a href="food.php">Food</a></li>
		<li><a href="costs.php">Costs</a></li>
		<li><a href="randamanager.php">Test</a></li>
		<li><a href="registration.php">Registration</a></li>
		<?php
		
		/*
			If user is logged in, the loggout link appears. 
			Otherwise the login link appears. Code by Aaron Werlen.
		*/
			if(isset($_SESSION['id'])) 
			{
				echo "<li><a href='logout.php'>Logout</a></li>";
			}else {
				echo "<li><a href='login.php'>Login</a></li>";
			}
		?>
	</ul>
    </nav>
