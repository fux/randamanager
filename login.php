<?php
    include("header.php");
        
    echo "<h1>Login</h1>";
    
//     echo "Session there? " . $_SESSION['projectStarted'] . "<br />";
    
    $showForm = true;

    if(isset($_GET['login'])) {
        $email = $_POST['email'];
        $passwort = $_POST['passwort'];
      
        $statement = $pdo->prepare("SELECT * FROM tbl_users WHERE email = :email");
        $result = $statement->execute(array('email' => $email));
        $user = $statement->fetch();
        $resultNo = $statement->rowCount(); 
        
        //Überprüfung des Passworts
        if ($resultNo == 1 && password_verify($passwort, $user['password'])) {
            $_SESSION['id'] = $user['id'];
            die('Login erfolgreich.');
            $showForm = false;
        } else {
            $errorMessage = "<b>E-Mail oder Passwort war ungültig</b><br />";
        }
        
    }
    

    if(isset($errorMessage)) {
        echo $errorMessage;
    }

    if ($showForm) {
        echo '<form action="?login=1" method="post">
            E-Mail-Adresse:<br />
            <input type="email" size="40" maxlength="250" name="email"><br />
            Dein Passwort:<br />
            <input type="password" size="40"  maxlength="250" name="passwort"><br />
 
            <input type="submit" value="Abschicken">';
        }
    
    include("footer.php");
?>
